/**
 * 
 */
package com.blazer.selenium.fitnesse.web;

import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.WebClient;

/**
 * @author ola
 * 
 */
public class CommonSimpleSeleniumTestFixture {
	private static final String RESPONSE_CODE_OF = "Response code of [";
	private static final String STR2 = "] ";
	private static final String IO_EXCEPTION = "IOException";
	private static final String FIRE_FOX_DRIVER_CREATED = "FireFoxDriver created";
	private static final String NO = "No ";
	private static final int TIME_OUT_IN_SECONDS = 30;
	private static final String FIREBUG_1_11_4_FX_XPI = "/firebug-1.11.4-fx.xpi";
	private static final String JS_ERROR_COLLECTOR_XPI = "/JSErrorCollector.xpi";
	private static final String FALSE = "false";
	private static final String ON = "on";
	private static final String TRUE = "true";
	private static final String CONSOLE = "console";
	private static final String EXTENSIONS_FIREBUG_DEFAULT_PANEL_NAME = "extensions.firebug.defaultPanelName";
	private static final String EXTENSIONS_FIREBUG_CONSOLE_ENABLE_SITES = "extensions.firebug.console.enableSites";
	private static final String EXTENSIONS_FIREBUG_ALL_PAGES_ACTIVATION = "extensions.firebug.allPagesActivation";
	private static final String EXTENSIONS_FIREBUG_SHOW_FIRST_RUN_PAGE = "extensions.firebug.showFirstRunPage";
	private static final String EXTENSIONS_FIREBUG_DELAY_LOAD = "extensions.firebug.delayLoad";
	private static final String EXTENSIONS_FIREBUG_SHOW_STACK_TRACE = "extensions.firebug.showStackTrace";
	private static final String STR = "]";
	private static final String OPEN_PAGE_HAS_NAVIGATED = "openPage has navigated [";
	private static final String JAVA_SCRIPT_ERRORS_DETECTED_ON_PAGE = "JavaScript errors detected on page [";
	private static final String COMPLETE = "complete";
	private static final String RETURN_DOCUMENT_READY_STATE = "return document.readyState";
	public String screenName;
	private static WebDriver webDriver;
	private static WebClient client;
	private static final Log LOGGER = LogFactory
			.getLog(CommonSimpleSeleniumTestFixture.class);

	public WebDriver getWebDriver() {
		if (webDriver == null) {
			FirefoxProfile ffProfile = new FirefoxProfile();
			try {
				ffProfile.addExtension(CommonSimpleSeleniumTestFixture.class,
						FIREBUG_1_11_4_FX_XPI);
				ffProfile.addExtension(CommonSimpleSeleniumTestFixture.class,
						JS_ERROR_COLLECTOR_XPI);
			} catch (IOException e) {
				LOGGER.error(IO_EXCEPTION, e);
				throw new RuntimeException(e);
			}

			ffProfile.setPreference(EXTENSIONS_FIREBUG_SHOW_STACK_TRACE, TRUE);
			ffProfile.setPreference(EXTENSIONS_FIREBUG_DELAY_LOAD, FALSE);
			ffProfile.setPreference(EXTENSIONS_FIREBUG_SHOW_FIRST_RUN_PAGE,
					FALSE);
			ffProfile
					.setPreference(EXTENSIONS_FIREBUG_ALL_PAGES_ACTIVATION, ON);
			ffProfile.setPreference(EXTENSIONS_FIREBUG_CONSOLE_ENABLE_SITES,
					TRUE);
			ffProfile.setPreference(EXTENSIONS_FIREBUG_DEFAULT_PANEL_NAME,
					CONSOLE);

			webDriver = new FirefoxDriver(ffProfile);

			LOGGER.info(FIRE_FOX_DRIVER_CREATED);
		}
		return webDriver;
	}

	public int getResponseCode() {
		try {
			if (client == null) {
				client = new WebClient();
				client.getOptions().setThrowExceptionOnFailingStatusCode(false);
				client.getOptions().setJavaScriptEnabled(false);
				client.getOptions().setCssEnabled(false);
				client.getOptions().setDoNotTrackEnabled(true);
				client.getOptions().setGeolocationEnabled(false);
				client.getOptions().setRedirectEnabled(true);
				client.getOptions().setThrowExceptionOnScriptError(false);
			}
			final int statusCode = client.getPage(screenName).getWebResponse().getStatusCode();
			LOGGER.info(RESPONSE_CODE_OF.concat(screenName).concat(STR2).concat(String.valueOf(statusCode)));
			return statusCode;
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

	public void waitForPageToLoad() throws Exception {
		waitForPageToLoad(getWebDriver());
	}

	public void waitForPageToLoad(WebDriver driver) {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						RETURN_DOCUMENT_READY_STATE).equals(COMPLETE);
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, TIME_OUT_IN_SECONDS);
		try {
			wait.until(expectation);
		} catch (Throwable error) {
			throw new RuntimeException(error);
		}
	}

	public void assertNoJavascriptErrors() {
		List<JavaScriptError> jsErrors = JavaScriptError.readErrors(webDriver);
		if (jsErrors.size() > 0) {
			throw new RuntimeException(
					JAVA_SCRIPT_ERRORS_DETECTED_ON_PAGE.concat(jsErrors
							.toString()));
		}
		LOGGER.info(NO.concat(JAVA_SCRIPT_ERRORS_DETECTED_ON_PAGE)
				.concat(screenName).concat(STR));
	}

	public void openPage() throws Exception {
		getWebDriver().get(screenName);
		waitForPageToLoad();
		LOGGER.info(OPEN_PAGE_HAS_NAVIGATED.concat(screenName).concat(STR));
	}

	public void tearDown() {
		webDriver.quit();
	}
}