/**
 * 
 */
package com.blazer.selenium.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.blazer.selenium.fitnesse.web.CommonSimpleSeleniumTestFixture;

/**
 * @author ola
 * 
 */
public class Example {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		CommonSimpleSeleniumTestFixture fixture = new CommonSimpleSeleniumTestFixture();
		
		//final WebDriver driver = fixture.getWebDriver();
		// And now use this to visit Google
		fixture.screenName = "http://www.google.com";
		fixture.openPage();
		fixture.getResponseCode();
		fixture.assertNoJavascriptErrors();

		// Find the text input element by its name
		WebElement element = fixture.getWebDriver().findElement(By.name("q"));

		// Enter something to search for
		element.sendKeys("Cheese!");

		// Now submit the form. WebDriver will find the form for us from the
		// element
		element.submit();
		fixture.waitForPageToLoad();
		
		// Check the title of the page
		System.out.println("Page title is: " + fixture.getWebDriver().getTitle());
		fixture.getResponseCode();
		fixture.assertNoJavascriptErrors();
		
		fixture.screenName = "http://www.xxxxxxgoogle.com";
		fixture.openPage();
		fixture.getResponseCode();
		fixture.assertNoJavascriptErrors();

		fixture.tearDown();
	}

}